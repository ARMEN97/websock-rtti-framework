unit Server;
interface
  uses
    Framework,

    System.SysUtils,
    System.IOUtils,
    System.Classes,
    Vcl.Dialogs,

    SuperObject,
    IdIPWatch,
    IdContext,
    IdWebsocketServer,
    IdServerWebsocketContext,
    IdCustomHTTPServer;

  Type
    TLocalServer = class
      constructor Create; overload;
      constructor Create(Port: Integer); overload;
      constructor Create(Port: Integer; AutoConnect: Boolean); overload;
      constructor Create(Port: Integer; WebPath: String); overload;
      constructor Create(Port: Integer; WebPath: String; AutoConnect: Boolean); overload;
      destructor Destroy;
      private
        FFramework : TRTTIFramework;
        FServer    : TIdWebsocketServer;
        FPort      : Integer;
        FConnected : Boolean;
        FWebPath   : String;
        procedure SetPort(const Value: Integer);

        procedure FCommandGet(AContext: TIdContext; ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
        procedure FMessage(const AContext: TIdServerWSContext; const aText: string);
        procedure FConnect(AContext: TIdContext);
        procedure FDisconnect(AContext: TIdContext);

        function GetIP: String;
        function ReadFile(FileName: String): WideString;
        procedure SetWebPath(const Value: String);
      public
        procedure Connect;
        procedure Disconnect;
        procedure Restart;
      published
        property Port: Integer read FPort write SetPort;
        property Server: TIdWebsocketServer read FServer;
        property Connected: Boolean read FConnected;
        property WebPath: String read FWebPath write SetWebPath;
    end;


implementation

{ TLocalServer }
{$REGION 'Constructor / Destructor'}
  constructor TLocalServer.Create;
  begin
    FServer := TIdWebsocketServer.Create(nil);
    FServer.KeepAlive        := True;
    FServer.AutoStartSession := True;
    FServer.OnCommandGet     := FCommandGet;
    FServer.OnMessageText    := FMessage;
    FServer.OnConnect        := FConnect;
    FServer.OnDisconnect     := FDisconnect;

    FFramework := TRTTIFramework.Create;
  end;
  constructor TLocalServer.Create(Port: Integer);
  begin
    Create;
    FServer.DefaultPort := Port;
  end;
  constructor TLocalServer.Create(Port: Integer; AutoConnect: Boolean);
  begin
    Create(Port);
    if AutoConnect then Connect;
  end;
  constructor TLocalServer.Create(Port: Integer; WebPath: String);
  begin
    Create(Port);
    SetWebPath(WebPath);
  end;
  constructor TLocalServer.Create(Port: Integer; WebPath: String;
    AutoConnect: Boolean);
  begin
    Create(Port, WebPath);
    if AutoConnect then Connect;
  end;
  destructor TLocalServer.Destroy;
  begin
    FFramework.Free; FFramework := nil;
    Disconnect;
    FServer.Free; FServer := nil;
  end;
{$ENDREGION}
{$REGION 'Control'}
  procedure TLocalServer.Connect;
  begin
    FServer.Active := True;
  end;
  procedure TLocalServer.Disconnect;
  begin
    FServer.Active := False;
  end;
  procedure TLocalServer.Restart;
  begin
    Disconnect;
    Connect;
  end;
  procedure TLocalServer.SetPort(const Value: Integer);
  begin
    FServer.DefaultPort := Value;
  end;
  procedure TLocalServer.SetWebPath(const Value: String);
  begin
    FWebPath := StringReplace(ExtractFilePath(ParamStr(0)), '\', '/', [rfReplaceAll, rfIgnoreCase]) + Value;
  end;
{$ENDREGION}
{$REGION 'Events'}
  procedure TLocalServer.FCommandGet(AContext: TIdContext;
    ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    var WebFile, WebFileExt: String;
  begin
    if ARequestInfo.URI <> '/' then begin
      WebFile    := FWebPath + ARequestInfo.URI;
      WebFileExt := ExtractFileExt(WebFile);

      AResponseInfo.ContentType := FServer.MIMETable.GetFileMIMEType(WebFile);
      if
        (WebFileExt = '.txt')  or
        (WebFileExt = '.html') or
        (WebFileExt = '.htm')  or
        (WebFileExt = '.js')   or
        (WebFileExt = '.css')
      then
        AResponseInfo.ContentText := ReadFile(WebFile)
      else
        AResponseInfo.ServeFile(AContext, WebFile);
    end else AResponseInfo.ContentText := ReadFile(FWebPath + '/index.html');
  end;
  procedure TLocalServer.FConnect(AContext: TIdContext);
  begin

  end;
  procedure TLocalServer.FDisconnect(AContext: TIdContext);
  begin

  end;
  procedure TLocalServer.FMessage(const AContext: TIdServerWSContext;
    const aText: string);
    var Obj             : ISuperObject;
        ParamItem       : TSuperAvlEntry;
        Parameters      : TStringList;
        i               : Integer;

        Request, Response, &Class, &Function: String;
  begin
    Obj := SO(aText);

    if Assigned(Obj) then begin
      Request := Obj.S['request'];

      if Request = 'invoke' then begin
        i := POS('.', Obj.S['function']);
        &Function := Obj.S['function'];
        &Class    := Copy(&Function, 0, i - 1);
        &Function := Copy(&Function, i + 1, Length(&Function));

        if Obj.S['parameters'] <> '' then begin
          Obj := Obj.O['parameters'];
          Parameters := TStringList.Create;
          for ParamItem in Obj.AsObject do begin
            Parameters.Values[ParamItem.Name]  := ParamItem.Value.AsString;
          end;
        end;

        Response := FFramework.InvokeMethod(&Class, &Function, Parameters);
        if Parameters <> nil then Parameters.Free;
        Server.SendMessageToAll(Response);
      end;
    end;
  end;
{$ENDREGION}
{$REGION 'Helpers'}
  function TLocalServer.ReadFile(FileName: String): WideString;
  begin
    Result := TEncoding.UTF8.GetString(TFile.ReadAllBytes(FileName));
    Result := StringReplace(Result, '[$SERVER_IP$]', GetIP, [rfReplaceAll, rfIgnoreCase]);
    Result := StringReplace(Result, '[$SERVER_PORT$]', FServer.DefaultPort.ToString, [rfReplaceAll, rfIgnoreCase]);
  end;
  function TLocalServer.GetIP: String;
begin
  Result := '127.0.0.1';
  With TIdIPWatch.Create(nil) do begin
    HistoryEnabled := False;
    if LocalIP <> '' then
      Result := LocalIP;
    Free;
  end;
end;
{$ENDREGION}

end.
