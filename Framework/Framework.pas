unit Framework;
interface
  uses
    FrameworkClasses,
    System.RTTI,
    System.SysUtils,
    System.Classes,
    SuperObject;

  Type
    TRTTIFramework = class
      public
        function InvokeMethod(ClassName, MethodName: String; Params: TStringList): String;
    end;



implementation

{ TRTTIFramework }

function TRTTIFramework.InvokeMethod(ClassName, MethodName: String;
  Params: TStringList): String;
  var Context: TRTTIContext;
      Instance: TRTTIInstanceType;
      Method: TRTTIMethod;
      pClass: TClass;
begin
  if ClassName = '' then begin Result := 'Class not found'; exit; end;
  if MethodName = '' then begin Result := 'Method not found'; exit; end;

  Context := TRTTIContext.Create;

  pClass := GetClass(ClassName);
  if pClass = nil then begin Result := 'Class not found'; exit; end;


  Instance := Context.GetType( pClass ).AsInstance;
  if Instance = nil then begin Result := 'Class not found'; exit; end;
  if not Instance.MetaclassType.InheritsFrom(TRTTIFrameworkClass) then begin Result := 'Unknown class'; exit; end;


  Method := Instance.GetMethod(MethodName);
  if Method = nil then begin Result := 'Method not found'; exit; end;

  if length(Method.GetParameters) = 0 then
    Result := Method.Invoke( Instance.MetaclassType.Create, []).AsString
  else
    Result := Method.Invoke( Instance.MetaclassType.Create, [ Params ]).AsString;

  Context.Free;
end;

end.