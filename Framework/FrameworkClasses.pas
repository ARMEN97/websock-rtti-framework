unit FrameworkClasses;
interface
  uses
    System.Classes,
    SuperObject;


  Type
    TRTTIFrameworkClass = class(TPersistent)
      protected
        Response: ISuperObject;
    end;

    TSettings = class(TRTTIFrameworkClass)
      Function &Set(P: TStringList): String;
    end;


implementation

{ TSettings }

function TSettings.&Set(P: TStringList): String;
begin
  Response := SO;
  Result := Response.AsJSon;
end;



initialization
  RegisterClass(TSettings);

end.